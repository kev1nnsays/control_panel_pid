import lcm #tutorial at lcm-proj.github.io/tut_python.html
import numpy as np
import matplotlib.pyplot as plt
import time
import math

errorBand = 200
notSettled = False
timeStart = 0 
timeStop = 0
settlingTime = 0 

import sys
sys.path.append('./lcmtypes')
from ctrlpanel import ctrlPanelData_t #imports the struct from ctrlPanelData_t.lcm

f1 = []
f2 = []
time = []
plt.ion() #turns on interactive mode so that each update calls draw()
figure1 = plt.figure() #contains top level container for all plot elements
plt.title('Control Panel Outputs')
plt.ylabel('PWM Value')

def plot_handler(channel, data): #defines a function that takes in channel and data
    msg = ctrlPanelData_t.decode(data)#decodes data according to struct from ctrlPanelData
    f1.append(msg.fader1)#append fader1 value from data packet to the f1 list (the fader1 name is defined in the ctrlPanelData_t.lcm struct (fader 1 is a value between 0 to 1800
    f2.append(msg.fader2)#append fader2 value from data packet to the f2 list
    time.append(msg.timestamp)#append data to the time list

    if(msg.fader1 > (msg.fader2+errorBand) or msg.fader1 < (msg.fader2 - errorBand)): #if fader1 is 		#outside error band of fader 2 (setpoint) then
	if notSettled is True:
	    timeStart = msg.timestamp
	    timeStop = msg.timestamp
	else:
	    timeStop = msg.timestamp
    
    figure1.canvas.draw() #call the draw function from matplotlib for figure1


lc = lcm.LCM() #initialize LCM with defaults
subscription = lc.subscribe("CTRL_PANEL_DATA", plot_handler)

try:
    while True:
        lc.handle()
	settlingTime = timeStart - timeStop
        plt.plot(time, f1, 'r')
        plt.plot(time, f2, 'b')
	plt.text(0.5,0.5, settlingTime, fontsize=12)


except KeyboardInterrupt:
    pass

lc.unsubscribe(subscription)
  

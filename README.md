# README #

Control panel for PID controller. Controls two sliders, one for user input and another that follows the user input. 
The target platform is a Beaglebone Black attached to a custom motor driver board and demonstrates feedback controls.

Results can be viewed at: 
https://www.youtube.com/playlist?list=PLo79OivWOV6czj94TnML7MMk3iFaNgqgR
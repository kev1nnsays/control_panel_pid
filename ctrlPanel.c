/**************************************************************
*
*	CtrlPanel Test
*
***************************************************************/

#include "./libBBB/libBBB.h"
#include "math.h"
#include "PID.h"

/* LIBRARIES NEEDED FOR LCM*/
#include <inttypes.h>
#include <lcm/lcm.h>
#include "lcmtypes/ctrlPanelData_t.h"  // ctrlPanel lcm type
#include "lcmtypes/pidData_t.h"  // PID lcm type

//Calibrated to board 4
//Sensor Limit calibration
//MEASURE FOR YOUR PANEL
#define F1MIN 0
#define F1MAX 1740
#define F2MIN 45
#define F2MAX 1745
#define P1MIN 0
#define P1MAX 1770
#define P2MIN 0
#define P2MAX 1770
#define P3MIN 0
#define P3MAX 1770

//set range of pot control
//YOU NEED TO DEFINE THESE
#define KPMAX 350 //P4 I2000 D10 seem like good ranges to play with // Raj Ron changed from 4 to 100
#define KIMAX 200
#define KDMAX 2 // Raj Ron changed from 0.004 to 0.4 to 1 to 2

struct Ctrlpanel_data{
  	int fader1;
  	int fader2;
  	int pots[3];
	int switches[3];
    	float  motorSignal;//Rounak changed from int to float
	float fader1_scaled;
	float fader2_scaled;
	float pots_scaled[3];
};

void InitializePanel(void){

 	/* Initiate Pin 66 Linked to Switch 1 */
    //INCLUDE OTHER INPUTS!
        initPin(66);
        setPinDirection(66, INPUT);
	
	/*Initiate Pin 67 Linked to Switch 2 - Kevin*/
        initPin(67);
        setPinDirection(67, INPUT);

	/*Initiate Pin 69 Linked to Switch 3 - Kevin*/
        initPin(69);
        setPinDirection(69, INPUT);

        /* Initiate Pin 45 that defines motor direction */
        initPin(45);
        setPinDirection(45, OUTPUT);
	
        /* Initiate A/D Converter */        
	initADC();
	

        /* Initiate PWM for Servo 1 */
        initPWM(SV1);
        setPWMPeriod(SV1H, SV1, 100000);
        setPWMPolarity(SV1H, SV1, 0);
        setPWMDuty(SV1H, SV1, 0);
        setPWMOnOff(SV1H, SV1, ON);

}

/*******************************************    
*       Scaling functions to scale AD readings
*       0 to 1 for pots and -1 to 1 for faders
*******************************************/

float ScaleInt2PosFloat(int input, int min, int max){
    //scale readings to between 0 and 1
    float x= (input-min)/((float)(max-min));
    
return (x);
}

float ScaleInt2Float(int input, int min, int max){
    //scale readings to between -1 and 1
    //IMPLEMENT ME!
    float mean=(max+min)/2;
float x=(input-mean)/(max-mean);
return(x);
}

/*******************************************    
*       Read values of the sensors
*       IMPLEMENT THIS FIRST TO TEST 
*******************************************/

void ReadPanelValues(struct Ctrlpanel_data * ctrlpanel){

	/* Read ADCs and give time after each reading to settle */
	ctrlpanel->fader1 = readADC(HELPNUM, A0);
	usleep(100);
	ctrlpanel->fader2 = readADC(HELPNUM, A1);
	usleep(100);
	ctrlpanel->pots[0] = readADC(HELPNUM, A2); 
	usleep(100);
	ctrlpanel->pots[1] = readADC(HELPNUM, A3); //Added - Kevin
	usleep(100);
	ctrlpanel->pots[2] = readADC(HELPNUM, A4); //Added - Kevin
	usleep(100);
    //ADD OTHER POTENTIOMETERS
	
	/* Scale the readings */
     //UNCOMMENT WHEN READY TO IMPLEMENT
	ctrlpanel->fader1_scaled = -ScaleInt2Float(ctrlpanel->fader1,F1MIN, F1MAX);	
	ctrlpanel->fader2_scaled = -ScaleInt2Float(ctrlpanel->fader2,F2MIN, F2MAX);	
	int i;	
	for(i=0;i<3;i++)
		ctrlpanel->pots_scaled[i] =  ScaleInt2PosFloat(ctrlpanel->pots[i],P1MIN, P1MAX);
    

	/* Read Switches position */
	ctrlpanel->switches[0] = getPinValue(66);
	ctrlpanel->switches[1] = getPinValue(67);//added - kevin
	ctrlpanel->switches[2] = getPinValue(69);//added - kevin
    //ADD ADDITIONAL SWITCHES 

}


void PrintPanelValues(struct Ctrlpanel_data * ctrlpanel){

	// CHANGE TO PRINT ALL INPUTS AND EVENTUAL OUTPUTS YOU ALSO WANT TO CHECK
	//printf("Motor Sig: %f fader2 scaled: %f fader1 scaled: %f Pot1: %f Pot2: %f Pot3: %f S1: %d, S2: %d, S3: %d \n ",ctrlpanel->motorSignal,
	//ctrlpanel->fader2_scaled,ctrlpanel->fader1_scaled,ctrlpanel->pots_scaled[0],ctrlpanel->pots_scaled[1],ctrlpanel->pots_scaled[2],ctrlpanel->switches[0],ctrlpanel->switches[1],ctrlpanel->switches[2]);
 	//printf("Motor Sig: %f Fader 1: %f Fader 2: %f\n ", ctrlpanel-> motorSignal, ctrlpanel->fader1, ctrlpanel->fader2);
	printf("Kp: %4f, Ki: %4f, Kd: %4f \n", (KPMAX * ctrlpanel->pots_scaled[0] *(1-ctrlpanel->switches[0])), (KIMAX * ctrlpanel->pots_scaled[1] * (1-ctrlpanel->switches[1])), (KDMAX * ctrlpanel->pots_scaled[2] * (1-ctrlpanel->switches[2]))  );

}

/*******************************************    
*       
*       CONTROLLERS
*
*******************************************/
void SimpleFaderDriver(struct Ctrlpanel_data * ctrlpanel){
	
	/* Drive Fader1 motor with Fader 2 position */
	/* Position of Fader 2 is reflected in the speed of Fader 1 */
	/* Fader 2 in middle = Fader 1 does not move */
	/* Fader 2 left = Fader 1 moves to left up to limit */
	/* Fader 2 right = Fader 1 moves to right up to limit */
	  
	ctrlpanel->motorSignal = 200*(ctrlpanel->fader2-1700/2);
      	setPinValue(45, ctrlpanel->motorSignal > 0);
      	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));

}

void BangBang(struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
	
	float inputerror;
	inputerror=(ctrlpanel->fader2)-(ctrlpanel->fader1);
	printf("Input error: %f" , inputerror);
	if((inputerror)>(0.01*1800))
	{
	ctrlpanel->motorSignal =100000*ctrlpanel->pots_scaled[0];//ctrlpanel->pots_scaled[0] * 1000;
	}
	else if (inputerror<(-0.01*1800))
	{
	ctrlpanel->motorSignal=-100000*ctrlpanel->pots_scaled[0];
	}
	else
	{
	ctrlpanel->motorSignal=0;
	}
	setPinValue(45, ctrlpanel->motorSignal >0);//(pin location, direciton)
	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));//amplitude of output
	
}

void PID(PID_t *pid, struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
	pid->pidInput = ctrlpanel->fader1_scaled;
	pid->pidSetpoint =  ctrlpanel->fader2_scaled;
	PID_SetTunings(pid,
        KPMAX * ctrlpanel->pots_scaled[0] * abs(1-(ctrlpanel->switches[0])),//Kp = offset+(range*pot_scaled)
        KIMAX * ctrlpanel->pots_scaled[1] * abs(1-(ctrlpanel->switches[1])), //Ki = offset+(range*pot_scaled)
        KDMAX * ctrlpanel->pots_scaled[2] * abs(1-(ctrlpanel->switches[2])) //Kd = offset+(range*pot_scaled)
	);
	PID_Compute(pid);//compute and actuate
	ctrlpanel->motorSignal=(pid->pidOutput)*-1000;//not sure why motor signal needs to be flipped  // Raj Ron changed from 100000
 	setPinValue(45, ctrlpanel->motorSignal >0);
	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));
}

void PID1(PID_t *pid, struct Ctrlpanel_data * ctrlpanel, float bodeSin){
    //IMPLEMENT ME!
	pid->pidInput = ctrlpanel->fader1_scaled;
	pid->pidSetpoint =  bodeSin;
	PID_SetTunings(pid,
        KPMAX * ctrlpanel->pots_scaled[0] * abs(1-(ctrlpanel->switches[0])),//Kp = offset+(range*pot_scaled)
        KIMAX * ctrlpanel->pots_scaled[1] * abs(1-(ctrlpanel->switches[1])), //Ki = offset+(range*pot_scaled)
        KDMAX * ctrlpanel->pots_scaled[2] * abs(1-(ctrlpanel->switches[2])) //Kd = offset+(range*pot_scaled)
	);
	PID_Compute(pid);//compute and actuate
	ctrlpanel->motorSignal=(pid->pidOutput)*-1000;//not sure why motor signal needs to be flipped  // Raj Ron changed from 100000
 	setPinValue(45, ctrlpanel->motorSignal >0);
	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));
}


/*******************************************    
*       
*       LCM Message Preparation
*
*******************************************/

void CtrlPanelDataLCM(struct Ctrlpanel_data * ctrlpanel, const char *channel, lcm_t * lcm){

        ctrlPanelData_t msg = {};
        
	    /* Prepare message to send over lcm channel*/
        msg.timestamp = utime_now();
        msg.fader1 = ctrlpanel->fader1;
        msg.fader2 = ctrlpanel->fader2;
        msg.pots[0] = ctrlpanel->pots[0];
        msg.pots[1] = ctrlpanel->pots[1];
        msg.pots[2] = ctrlpanel->pots[2];
        msg.switches[0] = ctrlpanel->switches[0];
        msg.switches[1] = ctrlpanel->switches[1];
        msg.switches[2] = ctrlpanel->switches[2];

        ctrlPanelData_t_publish(lcm, channel, &msg);
}

void PIDLCM(struct Ctrlpanel_data * ctrlpanel, PID_t *pid, const char *channel, lcm_t * lcm){
	
	pidData_t msg = {};
	
    /* Prepare message to send over lcm channel*/
    //IMPLEMENT ME!
}



/*******************************************    
*       
*       MAIN FUNCTION
* 
*******************************************/
int main()
{
	/*FILE *outfile;
	if((outfile = fopen("BodeNew.csv","w"))==NULL)
	{
	printf("error opening file");
	return -1;
	}
	double w=2,bodeSin,btime; //frequency for bode 
	*/ //For Bode plot data
	struct Ctrlpanel_data ctrlpanel;
        
	// For Controllers
	int64_t thisTime, lastTime, timeDiff;
	int updateRate = 10000;//changed rounak from 5000
	int64_t startTime=utime_now();//for bode

	// PID
	PID_t *pid;
	pid = PID_Init(200,0,0); //P, I, D values

        // LCM
        const char *channel1 = "CTRL_PANEL_DATA";   // LCM channel to publish messages
        const char *channel2 = "PIDDATA";   // LCM channel to publish messages
        lcm_t * lcm = lcm_create(NULL);
        if(!lcm) {
                return 1;
                }

	// Panel Initialization
	InitializePanel();

	/* Never ending loop */	
	while(1)
	{
		ReadPanelValues(&ctrlpanel);//scales values here itself, by calling the scale function
		
		
		

		// Control at a specified frequency
		thisTime = utime_now();
		
	//bodeSin=0.5*sin((w/1000000)*(thisTime-startTime));//For bode Plot data
	
                timeDiff = thisTime - lastTime;
                if(timeDiff >= updateRate){
			lastTime = thisTime;
			//SimpleFaderDriver(&ctrlpanel);
			//BangBang(&ctrlpanel);
			PID(pid,&ctrlpanel);
			//PID1(pid,&ctrlpanel,bodeSin);//Bode Plot data
			//fprintf(outfile,"%lf,%ld,%f,%lf\n",w,(long)(thisTime-startTime),pid->pidInput,bodeSin);//Bode Plot data
		
			
		}
//For Bode Plot data
/*
		if((thisTime-startTime)>(1000000*2*3.14*20/w))//multiply as time in microsec
		{
			w=w*2;
			printf("w %lf \n",w);
			startTime=thisTime;
		}
		if(w>100)
{
			printf("done writing Bode data\n");
			fclose(outfile);
			break;
		}
*/

		PrintPanelValues(&ctrlpanel);
		
		// UNCOMMENT TO INCLUDE LCM MESSAGE
		CtrlPanelDataLCM(&ctrlpanel,channel1,lcm);	
		//PIDLCM(&ctrlpanel,pid,channel2,lcm);	
	
	}

	return 0;
}

/*********************
*   PID.c
*   simple pid library 
*   pgaskell
**********************/

#include "PID.h"
#include <stdio.h>
#include <stdlib.h>


#define MAX_OUTPUT 100.0  // Raj  Ron changed from 1
#define MIN_OUTPUT -100.0 // Raj Ron changed from -1
#define ITERM_MIN -50.0
#define ITERM_MAX 50.0//Raj and Ron changed from 1 to 20

PID_t * PID_Init(float Kp, float Ki, float Kd) {
  // allocate memory for PID
  PID_t *pid =  malloc(sizeof(PID_t));

  //initalize values
  pid->pidInput = 0;
  pid->pidOutput = 0;
  pid->pidSetpoint = 0;
  pid->ITerm = 0;
  pid->prevInput = 0;

  // set update rate to 100000 us (0.1s)
  //IMPLEMENT ME!
  PID_SetUpdateRate(pid, 10000); // Rajat Rounak to 5000 from 100000
  
  //set output limits, integral limits, tunings
  printf("initializing pid...\r\n");
  PID_SetTunings(pid, Kp, Ki, Kd);//kevin - init PID values

  //IMPLEMENT ME!
  PID_SetOutputLimits(pid, MIN_OUTPUT, MAX_OUTPUT);//set motor limits
PID_SetIntegralLimits(pid,ITERM_MIN, ITERM_MAX);
  return pid;
}




void PID_Compute(PID_t* pid) {
  // Compute PID
  //IMPLEMENT ME!
	float error = pid->pidSetpoint - pid->pidInput;//P-term
	
	if(pid->ITerm > pid->ITermMax){ //prevent windup
		pid->ITerm =pid->ITermMax; // pid->ITermMax; //I-term
	}else if (pid->ITerm < pid->ITermMin){
		pid->ITerm = pid->ITermMin;// pid->ITermMin;
	}
		//pid->ITerm += error * 1/(pid->updateRate);//I-term
	pid->ITerm += error *(pid->updateRate)/1000000;//I-term changed rounak: update rate is in time not freq. divided by 1000000 to convert to sec 
	


	float derr = (error-pid->prevInput)*1000000/pid->updateRate;//Changed Rounak

	float sumOfTerms = pid->kp * error + pid->ki * pid->ITerm + pid->kd * derr;
//printf("Derr %f kd*derr %f PrevInput %f",derr,pid->kd*derr,pid->prevInput);
//printf("Update rate %d",pid->updateRate);
//printf("Iterm %f Ki*iterm: %f",pid->ITerm,pid->ITerm*pid->ki);	
printf("PID sum: %f, P term %f, I term:%f, D term:%f, Error: %f ",sumOfTerms,pid->kp*error,pid->ki*pid->ITerm,pid->kd*derr, error);
	
	if(sumOfTerms > pid->outputMax)
	{
		pid->pidOutput = pid->outputMax;
	}else if (sumOfTerms < pid->outputMin){
		pid->pidOutput = pid->outputMin;
	}else
	{
		pid->pidOutput = sumOfTerms;
		printf("PID");
	}
	pid->prevInput=error;//Rajat
	
	//pid->pidOutput = sumOfTerms;//Comment out if using if-else logic
}

void PID_SetTunings(PID_t* pid, float Kp, float Ki, float Kd) {
  //scale gains by update rate in seconds for proper units
  float updateRateInSec = ((float) pid->updateRate / 1000000.0);
// printf("Update rate %f",updateRateInSec); 
//set gains in PID struct
  pid->kp = Kp;
  pid->ki = Ki;
  pid->kd = Kd;
  //IMPLEMENT ME!
}

void PID_SetOutputLimits(PID_t* pid, float min, float max){
  //set output limits in PID struct
  //IMPLEMENT ME!
  pid->outputMin = min;
  pid->outputMax = max;
}

void PID_SetIntegralLimits(PID_t* pid, float min, float max){
  //set integral limits in PID struct
  //IMPLEMENT ME!
   pid->ITermMin = min;
   pid->ITermMax = max;
}

void PID_SetUpdateRate(PID_t* pid, int updateRate){
  //set integral limits in PID struct
  //IMPLEMENT ME!
  pid->updateRate = updateRate;
}




